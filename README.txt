Drupal Instagram Scrape Feed
----------------------------
Instagram Scrape Feed provides an Instagram feed in a block that
does not require authentication, access tokens or API keys. It
simply scrapes a public Instagram profile and returns up to 12 posts.


Why?
----
We build loads of sites, and getting an Instagram feed set up is
always a pain. You have to get access tokens, or API keys, and get
the clients login details so you can do this. We built this module
to create a really easy and simple way to display a feed in a block.


How does it work?
-----------------
The module will scrape a public Instagram profile page, and find the 12
latest posts. It will return the standard square image, "like" count,
"comment" count and the post caption. 
We got the basic idea from https://gist.github.com/cosmocatalano/4544576
and went our own way with it.


Configuration
-------------
You can control the username, number of posts to display, number of
columns to show, and trim the caption length.


How to use (Very Easy)
----------------------
Simply...

  - Place the module in your sites directory and enable as normal.
  - Configure the block in Structure > Blocks.
  - Enter the Instagram username you wish to display.
  - Configure the number of posts and number of columns to fit your
    theme and style.
  - Done! :-)


Whats the catch?
----------------
This module makes no attempt to store the Instagram post data, and
does not integrate with Entity API. If you need that functionality,
check out the other options out there.
This is just a dirty, quick, easy alternative for people who want
to get a feed up and running in a few clicks.

The Instagram profile you want to scrape must be set as public.
